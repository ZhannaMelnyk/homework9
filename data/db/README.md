# DB configurations
 
This folder contains the following files:

  + ***connection.js*** - to connect the ORM to the application; 
  + ***associations.js*** - to describe the associations between different database tables; 

### associations.js

**User** >-------1 **Role** : many to one

**User** 1-------< **Photo** : one to many

**User** >-------< **Achievement** : many to many

**User** 1-------< **Order** : one to many

**Order** >-------< **Service** : many to many

**Service** >-------1 **MeasurementUnit** : many to one
