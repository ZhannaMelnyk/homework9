# Models

This folder contains a file ***index.js*** - to connect associations to models and files of models for the appropriate tables.

The following tables are required for this project:

 - **Achievements**; 
 - **BlogPosts**; 
 - **MailingMessages**; 
 - **MeasurementUnits**; 
 - **Orders**; 
 - **OrderServices**; 
 - **Photos**; 
 - **Roles**; 
 - **Services**; 
 - **UserAchievements**; 
 - **Users**; 

#### Users

| **Field name** | **Data format** | **Description** |
| ------ | ------ | ------ |
| _id | uuid | |
| username | varchar(15) | |
| password | varchar(15) | |
| email | varchar(255) | |
| avatarLink | varchar(1000) | Link to the user's avatar |
| role | uuid | role id to which the user corresponds |
| accountBalance | money |  null, if it is an employee / balance of platform's credits in the ratio of 1 to 1 hryvnia at account, if it is an client. Cannot be lower than -100 |

#### Roles

| **Field name** | **Data format** | **Description** |
| ------ | ------ | ------ |
| _id | uuid | |
| name | uuid | Role name: client, admin, coach, barista |

#### Photos

| **Field name** | **Data format** | **Description** |
| ------ | ------ | ------ |
| _id | uuid | |
| photo | varchar(1000) | Link to the photo |
| userId | uuid | User id whom the photo belongs |

#### Achievements

| **Field name** | **Data format** | **Description** |
| ------ | ------ | ------ |
| _id | uuid | |
| name | varchar(50) | Achievement name |

#### UserAchievements

| **Field name** | **Data format** | **Description** |
| ------ | ------ | ------ |
| _id | uuid | |
| userId | uuid | User id whom the achievement belongs |
| achievementId | uuid | Achievement id |

#### Orders

| **Field name** | **Data format** | **Description** |
| ------ | ------ | ------ |
| _id | uuid | |
| createdAt | date | Date and time of order creation |
| userId | uuid | User id whom the order belongs |

#### MeasurementUnit

Types of services have their own unit of measurement, this table is used to store these units (for example, 100 grams for measuring food in the bar, 1 hour for the term of renting a wakeboard or 1 pc. when buying a bottle of water).

| **Field name** | **Data format** | **Description** |
| ------ | ------ | ------ |
| _id | uuid | |
| amount | int | Amount: 100 for grams, 1 for hours, 1 for pieces |
| name | varchar(50) | Name of units of measurement, such as grams, hours, pieces |

#### Services

Services can be different:

 - Purchase (for example, goods in a bar or season ticket); 
 - Booking (eg track); 
 - Rent (eg wakeboard); 

| **Field name** | **Data format** | **Description** |
| ------ | ------ | ------ |
| _id | uuid | |
| name | varchar(255) | Name of service |
| price | money | Price per unit of measurement |
| isAvailable | boolean | Is this service available |
| measurementUnit | uuid | id of the unit of measurement |
| isProduct | booleam | Indicates whether the service is a product. Due to this column, you can create a menu. |

#### OrderServices

| **Field name** | **Data format** | **Description** |
| ------ | ------ | ------ |
| _id | uuid | |
| serviceId | uuid | id of service |
| orderId | uuid | id of order in which this service is used |
| amount | int | Number of units of this service. For example, 2 when ordering a salad at the bar will mean that you bought 200 grams, and 3 when renting a wakeboard will mean that renting for 3 hours |
| startDate | date | null, if it a purchase / start date and time, if it is the renting |

#### BlogPosts

| **Field name** | **Data format** | **Description** |
| ------ | ------ | ------ |
| _id | uuid | |
| title | varchar(255) | Article title |
| text | varchar(2000) | Article text |
| createdAt | date | Date and time when the article was created |

#### MailingMessages

| **Field name** | **Data format** | **Description** |
| ------ | ------ | ------ |
| _id | uuid | |
| title | varchar(255) | Title of the article for mailing |
| text | varchar(2000) | Text of the article for mailing |
| createdAt | date | Date and time when the article for mailing was created |

The following model files are required according to these tables:

 - **Achievement.js**; 
 - **BlogPost.js**; 
 - **MailingMessage.js**; 
 - **MeasurementUnit.js**; 
 - **Order.js**; 
 - **OrderService.js**; 
 - **Photo.js**; 
 - **Role.js**; 
 - **Service.js**; 
 - **UserAchievement.js**; 
 - **User.js**; 

The db the diagram can be seen in the folder 'images' in the root.
