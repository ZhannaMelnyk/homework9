# Middlewares

This folder contains files of intermediate processing:

 - ***authenticationMiddleware.js*** -- for authenticating requests; 
 - ***roleMiddleware.js*** -- to check the user role if necessary; 
 - ***photoMiddleware.js*** -- to upload photo to cloud storage and get link to this one; 
 - ***menuMiddleware.js*** -- to create menu as .pdf file or QR-code. 
