# Services

This folder contains files for requesting to data layer and possible additional processing:

 - ***authService.js***:

    - **login** -- to receive a token (using the tokenHelper function) and request to the data layer to get the current user;


 - ***userService.js***:

    - **updateUser** -- requests for data layer to update user info;


 - ***photoService.js***:

    - **uploadPhoto** -- if the user is a client, then upload (change) the profile photo, and if the administrator, then write a link to a new photo of one of the users to the database;
    - **getAllUserPhotos** -- to get all photos of this user;


 - ***serviceService.js***:

    - **getAll** -- requests for data layer to get all services;
    - **getById** -- requests for data layer to get one service by id;
    - **create** -- requests for data layer to create new service; 
    - **updateById** -- requests for data layer to update service by id; 
    - **deleteById** -- requests for data layer to delete service by id; 
    - **getMenu** --  requests for data layer to get menu;
    - **getActivities** --  requests for data layer to get activities;


 - ***orderServices***:

    - **getAll** -- requests for data layer to get all orders;
    - **getById** -- requests for data layer to get one order by id;
    - **create** -- requests for data layer to create new order; 
    - **updateById** -- requests for data layer to update order by id; 
    - **deleteById** -- requests for data layer to delete order by id;
    - **getAllByUserId** -- requests for data layer to get all orders this user (e.g. for payment);


 - ***blogServices***:

    - **getAll** -- requests for data layer to get all blog articles (news);
    - **getById** -- requests for data layer to get one article by id;
    - **create** -- requests for data layer to create new article; 
    - **updateById** -- requests for data layer to update article by id; 
    - **deleteById** -- requests for data layer to delete article by id;


 - ***mailingServices***:

    - **getAll** -- requests for data layer to get all blog articles (news) for mailing;
    - **getById** -- requests for data layer to get one article by id for mailing;
    - **create** -- requests for data layer to create new article for mailing; 
    - **updateById** -- requests for data layer to update article by id for mailing; 
    - **deleteById** -- requests for data layer to delete article by id for mailing;


 - ***achievementServices***:

    - **getAll** -- requests for data layer to get all achievements;
    - **getById** -- requests for data layer to get one achievement;
    - **create** -- requests for data layer to create new achievement; 
    - **updateById** -- requests for data layer to update achievement; 
    - **deleteById** -- requests for data layer to delete achievement;
