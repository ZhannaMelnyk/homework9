# Data access layer
 
 This folder contains the following folders:

  + ***db*** - folder for connecting ORM and describing associations between tables; 
  + ***models*** - folder to describe the models of all database tables; 
  + ***migrations*** - for direct work with a database (creation of tables, editing, etc.); 
  + ***repositories*** - for communication between the application and the database.
