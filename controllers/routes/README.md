# Routes

This folder contains routing files:

 - ***index.js*** -- where all other routes are registered:

    - */api/auth* -- for authRoutes;
    - */api/user* -- for userRoutes;
    - */api/photo* -- for imageRoutes;
    - */api/services* -- for servicesRoutes;
    - */api/orders* -- for ordersRoutes;
    - */api/blog* -- for blogRoutes;
    - */api/mailing* -- for mailingRoutes;
    - */api/achievement* -- for achievementRoutes;
    

 - ***authRoutes.js*** -- routes for authorization and authentication:

    - POST */login* -- route for login. Employee accounts must be created in advance and assigned appropriate roles for them; 
    - POST */register* -- route for registration. After registration, the user automatically receives client role; 
    - GET */user* -- route for authentication (getting the current user); 


 - ***userRoutes.js*** -- routes for work with user:

    - PUT */* -- to update user info;
    - POST */avatar* -- to upload a user avatar (available only to the current user);


 - ***photoRoutes.js*** -- routes for work with photos:

    - GET */* -- to get all photos (available only to the admin);
    - GET */:id* -- to get one photo by id;
    - GET */:userId* -- to get all photos belonging to the user (for example, to be able to display all user  photos in his account);
    - POST */* -- to add the photo (available only to the admin);
    - DELETE */:id* -- to delete the photo by id (available only to the admin);


 - ***serviceRoutes.js***

    - GET */* -- to get the complete list of services (available only to the admin);
    - GET */activities* -- to get the list of entertainment services;
    - GET */activities/:id* -- to get details of the entertainment service by id;
    - GET */menu* -- to get the current menu;
    - GET */menu/pdf* -- to get the current menu in .pdf format;
    - GET */menu/qr* -- to get the current menu in QR-code format;
    - GET */menu/:id* -- to get details of the menu item by id;
    - POST */order/:id* -- to order the service (to add a service to a customer order);
    - POST */* -- to add new service (available only to the admin);
    - PUT */:id* -- to update the service by id (available only to the admin);
    - DELETE */:id* -- to delete the service by id (available only to the admin);


 - ***orderRoutes.js***

    - GET */* -- to get the list of orders (available only to the admin);
    - GET */:userId* -- to get the customer's own order (for the client) or to get some user's order by user id (for the admin);
    - POST */* -- to add new order;
    - DELETE */:userId* -- to delete the order by id (carried out by the client in case of cancellation of the order or payment);


 - ***blogRoutes.js***

    - GET */* -- to get all news (articles) at the blog;
    - GET */:id* -- to get one article by id;
    - POST */* -- to create new article (available only to the admin);
    - PUT */:id* -- to update the article by id (available only to the admin);
    - DELETE */:id* -- to delete the article by id (available only to the admin);


 - ***mailingRoutes.js***

    - GET */* -- to get all news (articles) for the mailing (available only to the admin);
    - GET */:id* -- to get one article by id for the mailing (available only to the admin);
    - POST */* -- to create new article for the mailing (available only to the admin);
    - PUT */:id* -- to update the article by id for the mailing (available only to the admin);
    - DELETE */:id* -- to delete the article by id for the mailing (available only to the admin);


 - ***achievementRoutes.js***

    - GET */* -- to get all achievements;
    - GET */:id* -- to get one achievement;
    - POST */* -- to create new type of achievements (available only to the admin);
    - PUT */:id* -- to update the achievement by id (available only to the admin);
    - DELETE */:id* -- to delete the achievement by id (available only to the admin);
