# Controllers

 This folder contains the following folders:

 - ***routes*** -- that contains routing files; 
 - ***middlewares*** -- that contains files of intermediate processing; 
 - ***services*** -- that contains files for requesting to data layer.
