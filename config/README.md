# Config
 
This folder contains configuration files for various third-party services, such as:

* database configuration; 
* configure the service to authentication/authorization; 
* configure the service to work with pictures (for example, Cloudinary); 
* configure the service to generate pdf (for example, pdf-creator-node, PDFKit); 
* configure the service to generate QR-code (for example, node-qrcode, qrcode-generator); 
* configure the payment service; 
* configure the service to mailing to customers; 
