# Repositories

This folder contains the communication files between the application and the database.

Main file -- ***baseRepository.js***. It contains a class **BaseRepository**, whose constructor accepts the model and has the following methods:
 - *getAll()* -- which returns all the entity of a table (model); 
 - *getById(id)* -- that takes the id and returns one entity of a particular table (model) that matches the id; 
 - *create(data)* -- that takes data and creates the entity of a certain table (model); 
 - *update(id, data)* -- that takes data and updates the corresponding (by id) entity of a certain table (model); 
 - *delete(id)* -- that takes the id and deletes the entity that matches the id.

For all models need to initialize an instance of the class **BaseRepository**.

So we have:

 - ***userRepository*** -- for work with the **User**; 
 
 - ***photoRepository*** -- for work with the **Photo**. Additional method: 

    - *getAllByUserId* -- to get all photos by the user with this id.

 - ***serviceRepository*** -- for work with the **Service**. Additional methods:

    - *getMenu* -- to get all services, where the *isProduct* column is equal `true` ;
    - *getActivities* -- to get all services, where the *isProduct* column is equal `false` ;

 - ***orderRepository*** -- for work with the **Order**. Additional method:

    - *getAllByUserId* -- get all client orders by the specified user id;

 - ***blogRepository*** -- for work with the **Blog**; 
 
 - ***mailingRepository*** -- for work with the **MailingMessage**; 
 
 - ***mailingRepository*** -- for work with the **MailingMessage**; 
