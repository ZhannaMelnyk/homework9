# Wake park

A multilayered architecture was chosen to develop the **Wake Park** system.
This architecture is used to structure programs that can be decomposed into groups of some subtasks located at certain levels of abstraction. Each layer provides services for the next higher layer:

 - presentation layer (frontend); 
 - application layer (controller Layer); 
 - business logic layer; 
 - data access layer.

Here are some benefits of multilayered architecture:

 1. It gives you the ability to update the technology stack of one layer, without impacting other areas of the application.
 2. You are able to scale the application up and out. A separate back-end tier, for example, allows you to deploy to a variety of databases instead of being locked into one particular technology. It also allows you to scale up by adding multiple web servers.
 3. It adds reliability and more independence of the underlying servers or services.
 4. It provides an ease of maintenance of the code base, managing presentation code and business logic separately, so that a change to business logic, for example, does not impact the presentation layer.

Also for this MVP system was chosen http protocol. In the future, it will be possible to add sockets for dynamic changes, but for MVC the HTTP protocol is enough

This folder should contain:

 - file ***package.json***, which will contain all the general data about the project, the dependencies required for the project and commands to manage the deployment of the project; 
 - entry point of the application -- file ***server.js***.
 - folder ***controllers*** (controller Layer); 
 - folder ***config*** (business logic layer); 
 - folder ***data*** (data access layer); 
 - folder ***helpers*** (for additional functions that will need to be used at different levels of the application).

***server.js*** file has:

 - import **express**; 
 - configuration for **express**; 
 - test query to the database to make sure it is working; 
 - enable listening on the host and port connection.

Full project structure can be seen in the folder 'images' in the root.